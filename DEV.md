# Umwelt_22

# Creating a new entry: High level explanation:
The JS on client side should first make an AJAX POST request to /mstr to get a 
new master index that ties the different entries together. Once a master index has
been acquired, that master index is tied to each entry type that gets sent to the
different URLs via other requests. This way, /diprec gets dip stuff and /modrec gets 
mood stuff. These get sent down differetn paths to the db function, but can be 
tied back together once they're there.

# Developer Addons:
Developers may need to add new categories beyond "dip", "kratom", and "fuel", so work
for this should be started in the next version. The end goal would be to allow a
developer or admin to create templates that the program reads in and translates to 
database tables, html, and structures so that he doesn't have to change any actual 
source code.