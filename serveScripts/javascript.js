      var globalMasterId  = 0;
      var globalTimeRef   = 0;
      var globalUserId    = 66642;

      //MouthMap x and y coordinates for dip placement.
      var mmx = 0.0; 
      var mmy = 0.0;

      function myShittyNotEvenAHash(input){
          fin = input.length;
          tmp = 0;
          for(var i=0; i < input.length; i++){
            tmp = input.charCodeAt(input[i]);
            tmp = tmp%10;
            fin = fin+tmp;
          }
          return fin
      }

      /*
        Send off to the MySQL server, allow it to set a new id via auto-increment
        on the master table and hand that back so that we have a master index to 
        target with the new data.
      */
      function setNewId(input){



        newMST ={
          upload_id:input,
          user_id: 0,
          time_id: 0,
        }

        sendoff = JSON.stringify(newMST);
        //Sendoff master init and set the master index.
        $.ajax({
          url: '/mstr',
          type: 'POST',
          data: sendoff,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: false,
          success: function(msg){
            mastIndx = msg["NDX"];
            alert("Error:", msg)
            //console.log("LAST INDEX IS: ", msg.NDX, mastIndx, msg);
          },
          error: function(a,b,c){
            alert("Error:",c)

            //console.log("COULD NOT GET MASTER INDEX IN SETNEWID"+a+b+c);
          }
        });
      }
      
      function getNewId(){
        console.log("Starting getNewId");
        var mastIndx = 200;

        //Get master index first=====================================================
        $.ajax({
          url: '/mstr',
          type: 'POST',
          data: "{}",     //Sending blank data means that there won't be a new entry into the DB yet so that we're not creating a bunch of blank entries.
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: false,
          success: function(msg){
            //console.log("MSG:", msg);
            mastIndx = msg["NDX"];
            //console.log("MASTER INDEX IS: ")
            //console.log(msg.NDX,mastIndx,msg)
          },
          error: function(a,b,c){
            //console.log("COULD NOT GET MASTER INDEX IN GETNEWID():"+a+b+c,);
          }
        });

        globalMasterId = mastIndx;
        //console.log("GMI:",globalMasterId)
        return mastIndx;
      }

      /*
        Similar to set/getNewId, except with a timestamp. 
      */
      function setNewTimeEntry(){
        retVal = 0;
        
        timeEntrySendoff = {
          unix_timestamp: Date.now(),
          datetime:       Date().toLocaleString(),
          masterId:       globalMasterId
        }

        jdat = JSON.stringify(timeEntrySendoff);
        //console.log("setNewTimeEntry: JDAT IS:",jdat);

        //Send off new time entry for db and get the index back.
        $.ajax({
          url: '/timrec',
          type: 'POST',
          data: jdat,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: false,

          success: function(msg){
            retVal = msg["TStamp"];
          },

          error: function(a,b,c){
            //console.log("setNewTimeEntry:  ERROR: Error sending timeEndrySendoff: "+a+" "+b+" "+c);
            //console.log("setNewTimeEntry:  ERROR: jdat:", jdat);
          }
        });

        return retVal;

      }

      //Return json stringified dip data.
      function getDipData(){
        
        const brand     = document.getElementById("dipInput").value;
        const brandHash = myShittyNotEvenAHash(brand);
        const din       = document.getElementById("isIn");
        //console.log("din is ",din);

        if(din.checked){
          isIn = "T"
        }else{
          isIn = "F"
        }

        var sendObj   = {
	        ID:         globalMasterId,
	        Timestamp:  globalTimeRef,
	        MouthMapX:  mmx,
          MouthMapY:  mmy,
          LocationId: 42,
          BrandId:    brandHash,
          MasterID:   globalMasterId,
          dipIn:     isIn,
        };

        //console.log("SendObj", sendObj);

        return JSON.stringify(sendObj);
      }//End getDipData function



      //Return json stringified kratom data.
      function getKratData(){

        //Get kratom brand
        const kratBrandText = document.getElementById("kratom_brand").value
        const kratBrandHash = myShittyNotEvenAHash(kratBrandText)

        var color = ""


        //Get Vein Color
        const colorvals = document.getElementsByName("vein_color");
        for(i=0;i<colorvals.length;i++){
          if(colorvals[i].checked){
            //console.log("colorvals:",colorvals[i].value)
            color = colorvals[i].value
          }
        }

        const ml = parseInt(document.getElementById("ml_scoop").value)

        sendobj = {
          id:             0,
          MilliterslUsed: ml,
          GramsUsed:      0,
          LocationId:     0,
          TimestampLink:  globalTimeRef,
          BrandId:        kratBrandHash,
          VeinColor:      color,
          MasterId:       globalMasterId
        }

        //console.log("kratdat:", sendobj)

        return JSON.stringify(sendobj)
      }//End getKratData

      //Gather and stringify JSON fuel data
      function getGasData(){
        const ppgDat  = document.getElementById('ppg').value;
        const compNam = document.getElementById('statComp').value;
        const odoDat  = document.getElementById('odo').value;
        const fgrade  = document.getElementById('grade').value;
        const gallons = document.getElementById('gallons').value;
        const totPric = document.getElementById('totalPrice').value;

        retObj = {
          ID: globalMasterId,
          Grade: fgrade,
          PricePerGallon: parseFloat(ppgDat),
          Gallons: parseFloat(gallons),
          Odometer: parseFloat(odoDat),
          GasStationCompany: compNam,
          Price: parseFloat(totPric),
          LocationID: 0,
          TimestampLink: globalTimeRef,
          MasterId: globalMasterId,
        }

        if (retObj.Grade == ""){
          console.log("Grade is bla:", "")
        }

        //return JSON.stringify(retObj);
        return retObj
      }
 
      function getCursorPostition(canvas, evt){

        console.log("Starting getCursorPostition");
        var rect  = canvas.getBoundingClientRect();
        var ctx   = canvas.getContext("2d");

        var w     = rect.width;
        var h     = rect.height;


        const obj = {
          x: (evt.clientX - rect.left)/w,
          y: (evt.clientY - rect.top)/h
        };

        mmx = obj.x
        mmy = obj.y

        xPos = canvas.width  * obj.x;
        yPos = canvas.height * obj.y;

        ctx.beginPath();
        ctx.strokeStyle = 'red';
        ctx.fillStyle = 'blue';
        ctx.arc(xPos, yPos, canvas.width * 0.01, 0, 2*Math.PI);
        ctx.fill();
        ctx.stroke();
        console.log(obj);
        return obj;
      }

      /*
        Send off to the database manager to tap the master
        table and get it to increment by one, then get that from the
        server. 
      */
      function handleMaster(){

        newMST = {
          upload_id: 0,
          user_id:   0,
          time_id:   globalTimeRef,
        }

        sendoff = JSON.stringify(newMST);

        $.ajax({
          url: '/mstr',
          type: 'POST',
          data: sendoff,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: false,
          success: function(msg){
            mastIndx = msg["NDX"];
            //console.log("HM AJAX: LAST INDEX IS: ", msg.NDX, mastIndx, msg);
            globalMasterId = msg["NDX"]-1;
            //console.log("HM AJAX: handleMaster got:", msg["NDX"]);
          },
          error: function(a,b,c){
            //console.log("HM AJAX: COULD NOT GET MASTER INDEX IN SETNEWID"+a+b+c);
          }
        });
      
        //console.log("HM AJAX: GMI is now:", globalMasterId);
        return 
      }

      function getNoteData(){
        const nTitle = document.getElementById("noteTitle").value;
        const nBody  = document.getElementById("noteBody").value;

        //console.log("nTitle:",nTitle)
        //console.log("nBody:",nBody)

        retObj = {
          note_title: nTitle,
          note_body: nBody
        }

        return JSON.stringify(retObj);

        
      }

      //Gather up data from forms and send off to receivers
      function sendData(){
        globalTimeRef = setNewTimeEntry()
        document.getElementById("showMaster").innerHTML = globalMasterId

        //HANDLE DIP DATA========================================================================================
        const dipdata = getDipData();
        //console.log("Dipdata:", dipdata)
        $.ajax({
          url: '/diprec',
          type: 'POST',
          data: dipdata,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: true,
          success: function(msg){
            //console.log("Good message: "+msg["Message"]);
            //console.log("Senddat:", dipdata);
          },
          error: function(a,b,c){
            //console.log("Error sending testDipData: "+a+" "+b+" "+c);
            //console.log("senddat:", dipdata);
          }
        })

        //HANDLE KRATOM DATA ======================================================================================
        const kratDat = getKratData()
        //console.log("KratDat:", kratDat)
        $.ajax({
          url: '/krarec',
          type: 'POST',
          data: kratDat,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: true,
          success: function(msg){
            //console.log("Good send on kratom data.", msg["Message"])
          },
          error: function(a,b,c){
            //console.log("Error sending kratom data:", c,"data:", kratDat)
          }
        })

        //NANHDL NTOE KATING======================================================================================
        //const noteData = getNoteData()
        const nTitle = document.getElementById("noteTitle").value;
        const nBody  = document.getElementById("noteBody").value;

        //console.log("nTitle:",nTitle)
        //console.log("nBody:",nBody)

        noteDataText = {
          id: globalMasterId,
          title: nTitle,
          body: nBody,
          time: globalTimeRef,
          location: 0,
          MasterId: globalMasterId,
        } 

        const noteData = JSON.stringify(noteDataText)
        console.log("NOTE DATA:", noteData)
        $.ajax({
          url: '/notrec',
          type: 'POST',
          data: noteData,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: true,
          success: function(msg){
            //console.log("Good send on test note data.", msg["Message"])
          },
          error: function(a,b,c){
            //console.log("Error sending test note data:", a,b,c,"data:", noteData)
          }
        })

        
        //HANDLE FUEL DATA=========================================================================================
        const sendFuelDat = getGasData();
        if (sendFuelDat.Grade == ""){
          console.log("DEAD FUEL. DUMPING.")
        }else{
          console.log("Fuel is:", sendFuelDat["Grade"])
          $.ajax({
            url: '/fuelrec',
            type: 'POST',
            data: JSON.stringify(sendFuelDat),
            contentType: 'application/json; charset=utf8',
            dataType: 'json',
            async: true,
            success: function(msg){
              console.log("Good news everyone!"+msg["Message"]);
              console.log("Sent: ", + sendFuelDat)
            },
            error: function(a,b,c){
              //console.log("ERROR sending testFuelData: " + a + " " + b + " "+c);
              console.log("Error sending sendfueldat:", c);
              console.log("Tried sending: " + sendFuelDat);
            }
          });
          console.log("about to send: "+ sendFuelDat);
        }
        


      }

            

      /*
        Initialize the JS and the data needed for the page to run. INIT ///// INIT ///// INIT ///// INIT ///// INIT ///// INIT /////
      */
        function pageInit(){

          handleMaster();
          
          var mouthmapimg = document.getElementById("mouthMap");
          var canva = document.getElementById("dipMapCanvas");
          var ctx = canva.getContext("2d");
  
          canva.width  = mouthmapimg.width; 
          canva.height = mouthmapimg.height;
  
          ctx.drawImage(mouthmapimg,0,0,canva.width, canva.height);
  
          canva.addEventListener('mousedown',function(e){
            console.log(getCursorPostition(canva, e));
          });
        }
  
  