      var globalMasterId  = 0;
      var globalTimeRef = 0;
      var globalUserId    = 66642;

      //MouthMap x and y coordinates for dip placement.
      var mmx = 0.0; 
      var mmy = 0.0;

      function myShittyNotEvenAHash(input){
          fin = 0;
          for(var i=0;i<input.length;i++){fin = fin+input.charCodeAt(i);}
          last = fin/ input.length;
          return Math.round(last);
      }

      /*
        Send off to the MySQL server, allow it to set a new id via auto-increment
        on the master table and hand that back so that we have a master index to 
        target with the new data.
      */
      function setNewId(input){

        newMST ={
          upload_id:input,
          user_id: 0,
          time_id: 0,
        }

        sendoff = JSON.stringify(newMST);

        $.ajax({
          url: '/mstr',
          type: 'POST',
          data: sendoff,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: true,
          success: function(msg){
            mastIndx = msg["NDX"];
            console.log("LAST INDEX IS: ", msg.NDX, mastIndx, msg);
          },
          error: function(a,b,c){
            console.log("COULD NOT GET MASTER INDEX IN SETNEWID"+a+b+c);
          }
        });
      }
      
      function getNewId(){
        console.log("Starting getNewId");
        var mastIndx = 200;

        //Get master index first=====================================================
        $.ajax({
          url: '/mstr',
          type: 'POST',
          data: "{}",     //Sending blank data means that there won't be a new entry into the DB yet so that we're not creating a bunch of blank entries.
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: true,
          success: function(msg){
            console.log("MSG:", msg);
            mastIndx = msg["NDX"];
            console.log("MASTER INDEX IS: ")
            console.log(msg.NDX,mastIndx,msg)
          },
          error: function(a,b,c){
            console.log("COULD NOT GET MASTER INDEX IN GETNEWID():"+a+b+c,);
          }
        });

        globalMasterId = mastIndx;
        console.log("GMI:",globalMasterId)
        return mastIndx;
      }

      /*
        Similar to set/getNewId, except with a timestamp. 
      */

      function setNewTimeEntry(){
        retVal = 0;
        
        timeEntrySendoff = {
          unix_timestamp: Date.now(),
          datetime:       Date().toLocaleString(),
          masterId:       globalMasterId
        }
        console.log("setNewTimeEntry: Master ID in setNewTimeEntry:", globalMasterId);
        console.log("setNewTimeEntry: PARSING:", timeEntrySendoff);

        jdat = JSON.stringify(timeEntrySendoff);
        console.log("setNewTimeEntry: JDAT IS:",jdat);

        //Send off new time entry for db and get the index back.
        $.ajax({
          url: '/timrec',
          type: 'POST',
          data: jdat,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: false,

          success: function(msg){
            console.log("setNewTimeEntry: SUCCESS: Good message creating timestamp entry: "+msg["Message"]);
            console.log("setNewTimeEntry: SUCCESS: Timestamp sendoff:", timeEntrySendoff);
            console.log("setNewTimeEntry: SUCCESS: Timestamp reply:", msg["TStamp"]);
            retVal = msg["TStamp"];
          },

          error: function(a,b,c){
            console.log("setNewTimeEntry:  ERROR: Error sending timeEndrySendoff: "+a+" "+b+" "+c);
            console.log("setNewTimeEntry:  ERROR: jdat:", jdat);
          }
        });

        return retVal;

      }

      //Return json stringified dip data.
      function getDipData(){
        var brand     = document.getElementById("dipInput").value;
        var brandHash = myShittyNotEvenAHash(brand);
        const din     = document.getElementById("isIn").value;
        var isIn      = false;

        if(din=="on"){
          isIn = true;
        }else{
          isIn = false;
        }
        

        console.log("brandhash:", brandHash);

        var sendObj   = {
	        ID:         globalMasterId,
	        Timestamp:  globalTimeRef,
	        MouthMapX:  mmx,
          MouthMapY:  mmy,
          LocationId: 42,
          BrandId:    brandHash,
          MasterID:   globalMasterId,
          dip_in:     isIn,
        };

        console.log("SendObj", sendObj);

        return JSON.stringify(sendObj);
      }//End getDipData function

      function getCursorPostition(canvas, evt){
        var rect = canvas.getBoundingClientRect();
        var ctx = canvas.getContext("2d");

        var w = rect.width;
        var h = rect.height;


        const obj = {
          x: (evt.clientX - rect.left)/w,
          y: (evt.clientY - rect.top)/h
        };

        xPos = canvas.width  * obj.x;
        yPos = canvas.height * obj.y;

        ctx.beginPath();
        ctx.strokeStyle = 'red';
        ctx.fillStyle = 'blue';
        ctx.arc(xPos, yPos, canvas.width * 0.01, 0, 2*Math.PI);
        ctx.fill();
        ctx.stroke();
        return obj;
      }

      /*
        Send off to the database manager to tap the master
        table and get it to increment by one, then get that from the
        server. 
      */
      function handleMaster(){

        console.log("STARTING HANDLEMASTER FUNCTION")

        newMST = {
          upload_id: 0,
          user_id:   0,
          time_id:   0,
        }

        sendoff = JSON.stringify(newMST);

        $.ajax({
          url: '/mstr',
          type: 'POST',
          data: sendoff,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: false,
          success: function(msg){
            mastIndx = msg["NDX"];
            console.log("HM AJAX: LAST INDEX IS: ", msg.NDX, mastIndx, msg);
            globalMasterId = msg["NDX"];
            console.log("HM AJAX: handleMaster got:", msg["NDX"]);
          },
          error: function(a,b,c){
            console.log("HM AJAX: COULD NOT GET MASTER INDEX IN SETNEWID"+a+b+c);
          }
        });
      
        console.log("HM AJAX: GMI is now:", globalMasterId);
        return 
      }

      //Gather up data from forms and send off to receivers
      function sendData(){

        console.log("starting sendData function.");

        var timeIndex = setNewTimeEntry()
        console.log("TIMEINDEX:", timeIndex)

        //GET DIPMAP LOCATIONS=====================================================================
          //Future

        //HANDLE DIP DATA========================================================================================
        const dipdata = getDipData();
        $.ajax({
          url: '/diprec',
          type: 'POST',
          data: dipdata,
          contentType: 'application/json; charset=utf-8',
          dataType: 'json',
          async: true,
          success: function(msg){
            console.log("Dip data sendoff: Good message: "+msg["Message"]);
            console.log("Dip data sendoff: Senddat:", dipdata);
          },
          error: function(a,b,c){
            console.log("Dip data sendoff: Error sending testDipData: "+a+" "+b+" "+c);
            console.log("Dip data sendoff: senddat:", dipdata);
          }
        });

        //HANDLE FUEL DATA=========================================================================================
        const testFuelData={
          ID:                128,
          Grade:             "prem",
          Gallons:           12.34,
          PricePerGallon:    24.489,
          Odometer:          123423453456.4,
          GasStationCompany: "Keith's", 
          Price:             458372.44,
          LocationID:        0,
          TimestampLink:     0,
          MasterID:          globalMasterId,
        }

        const sendFuelDat = JSON.stringify(testFuelData);
        $.ajax({
          url: '/fuelrec',
          type: 'POST',
          data: sendFuelDat,
          contentType: 'application/json; charset=utf8',
          dataType: 'json',
          async: true,
          success: function(msg){
            console.log("Good news everyone!"+msg["Message"]);
            console.log("Sent: ", + sendFuelDat)
          },
          error: function(a,b,c){
            //console.log("ERROR sending testFuelData: " + a + " " + b + " "+c);
            console.log("Error sending testFuelData:", c);
            console.log("Sent: " + sendFuelDat);
          }
        });

      }

            

      /*
        Initialize the JS and the data needed for the page to run. INIT ///// INIT ///// INIT ///// INIT ///// INIT ///// INIT /////
      */
        function pageInit(){
        
          handleMaster();
          console.log("New globalMasterId:", globalMasterId);
          
          var mouthmapimg = document.getElementById("mouthMap");
          var canva = document.getElementById("dipMapCanvas");
          var ctx = canva.getContext("2d");
  
          canva.width  = mouthmapimg.width; 
          canva.height = mouthmapimg.height;
  
          ctx.drawImage(mouthmapimg,0,0,canva.width, canva.height);
  
          canva.addEventListener('mousedown',function(e){
            console.log(getCursorPostition(canva, e));
          });
        }
  
  