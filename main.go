package main

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"

	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"path"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type masterStr struct {
	upload_id uint64
	user_id   uint64
	time_id   uint64
}

type dipStruct struct {
	ID         uint64
	BrandId    uint64
	MouthMapX  float64
	MouthMapY  float64
	LocationId uint64
	Timestamp  uint64
	UserId     uint64
	MasterID   uint64
	DipIn      string
}

type fuelStruct struct {
	ID                uint64
	Grade             string
	Gallons           float32
	PricePerGallon    float32
	Odometer          float32
	GasStationCompany string
	Price             float32
	LocationID        uint64
	TimestampLink     uint64
	MasterID          uint64
}

type kratomStruct struct {
	ID             uint64
	MilliterslUsed uint32
	GramsUsed      uint32
	LocationId     uint64
	TimestampLink  uint64
	BrandId        uint64
	VeinColor      string
	MasterID       uint64
}
type moodStruct struct {
	ID               uint64
	Anxiety          float64
	Energy           float64
	Anger            float64
	Hope             float64
	LIbido           float64
	Drive            float64
	SuicidalIdeation float64
	Longing          float64
	Outlook          float64
	LocationID       uint64
	Timestamp        uint64
	MasterID         uint64
}
type noteStruct struct {
	ID       uint64
	Title    string
	Body     string
	Time     uint64
	Location uint64
	MasterID uint64
}
type reciptStruct struct {
	ID                 uint64
	TotalCost          float64
	ReciptElementsJson string
	ReciptImage        []byte
	Location           uint64
	TimeStamp          uint64
	MasterID           uint64
}
type locationStruct struct {
	ID                uint64
	Latitude          float64
	Longitude         float64
	Altitude          float64
	PercisionLatitude float64
	PercisionLongitde float64
	percisionAltitude float64
	MasterID          uint64
}
type timeStruct struct {
	ID             uint64
	Unix_Timestamp uint64
	DateTime       string
	MasterID       uint64
}

//This exists to allow us to send all these types down the same pipe
type jsonElem struct {
	DipStruct      dipStruct
	FuelStruct     fuelStruct
	KratomStruct   kratomStruct
	MoodStruct     moodStruct
	ReciptStruct   reciptStruct
	Note           noteStruct
	LocationStruct locationStruct
	TimeStruct     timeStruct
}

func main() {

	entryChan := make(chan jsonElem)
	go databaseManager(entryChan)
	go runWeb(entryChan)

	for {
		time.Sleep(10000 * time.Hour)
	}
}

func runWeb(output chan jsonElem) {

	root := genRoot()
	mstr := getMstr()
	tstp := handleTimestamps()

	serveJS := serveJS(1) //Serve the 1st party javascript
	//serveJQ := serveJS(2) //Serve the jquery source

	http.HandleFunc("/diprec", templFunc(output, dipStruct{}))
	http.HandleFunc("/krarec", templFunc(output, kratomStruct{}))
	http.HandleFunc("/modrec", templFunc(output, moodStruct{}))
	http.HandleFunc("/notrec", templFunc(output, noteStruct{}))
	http.HandleFunc("/recrec", templFunc(output, reciptStruct{}))
	http.HandleFunc("/locrec", templFunc(output, locationStruct{}))
	http.HandleFunc("/fuelrec", templFunc(output, fuelStruct{}))

	http.HandleFunc("/scripts.js", serveJS)
	//http.HandleFunc("/localjq")

	http.HandleFunc("/imgs/", serveImgs())
	http.HandleFunc("/timrec", tstp)
	http.HandleFunc("/mstr", mstr)
	http.HandleFunc("/", root)

	http.ListenAndServe(":8086", nil)
}

func serveImgs() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/octet-stream")

		fileName := "./" + path.Base(r.URL.Path)
		fileBuff, _ := ioutil.ReadFile(fileName)

		w.Write(fileBuff)

	}
}

func serveJS(fileIndicator int) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var fName string = ""

		if fileIndicator == 1 {
			fName = "./serveScripts/javascript.js"
		} else if fileIndicator == 2 {
			fName = "./serveScripts/jquery.js"
		}

		fileData, err := ioutil.ReadFile(fName)
		if err != nil {
			//fmt.Println("ERROR in serving scripts:", err)
		}

		w.Header().Set("Content-Type", "application/javascript")
		w.Write(fileData)

	}
}

func handleTimestamps() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		tsDec := timeStruct{}

		if err := json.NewDecoder(r.Body).Decode(&tsDec); err != nil {
			handleError(err, w)
		} else {
			//fmt.Println("Rec'd timestruct:", tsDec)
		}

		db, err := sql.Open("mysql", "root:55975597@tcp(173.211.12.46:3306)/umwelt_22")
		if err != nil {
			//fmt.Println("Err starting db connection:", err)
		}

		tsDec.DateTime = strings.Replace(tsDec.DateTime, "(", "", -1)
		tsDec.DateTime = strings.Replace(tsDec.DateTime, ")", "", -1)

		execStmt := fmt.Sprintf("INSERT INTO time (unix_timestamp,datetime,master) VALUES (%d,\"%s\",%d);", tsDec.Unix_Timestamp, tsDec.DateTime, tsDec.MasterID)

		rs, err := db.Exec(execStmt)
		if err != nil {
			handleError(err, w)
		}

		id, err := rs.LastInsertId()

		//fmt.Printf("\n\nTIME STUFF:\n================\nLAST INDEX ON TIME:%d\nERROR:%v\n===========\n", id, err)
		retval := fmt.Sprintf("{\"TStamp\":%v}", id)
		w.Write([]byte(retval))

	}
}

/*
As it stands, we will have a masterId being used on every number, but every
other number will actually link to worthwhile data. This needs to be fixed.
We shouldn't be filling the database with half-assed entries.
*/
func getMstr() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		lms := masterStr{}
		var it uint64 = 0
		var masterIndexLast uint64 = 0
		var retarr []byte = []byte{}

		//Get connection to the database
		db, err := sql.Open("mysql", "root:55975597@tcp(173.211.12.46:3306)/umwelt_22")
		if err != nil {
			//fmt.Println("Err starting db connection:", err)
		}

		//Get the userId and timestampId to associate with this masterid
		if err := json.NewDecoder(r.Body).Decode(&lms); err != nil {
			handleError(err, w)
		} else {
			//fmt.Println("Good decode:", lms)
		}

		//Create a new entry into the table for master ids.
		iterexec := fmt.Sprintf("INSERT INTO upload_master_nodes (user_id) VALUES (%d)", lms.user_id)
		n, exerr := db.Exec(iterexec)

		if exerr != nil {
			fmt.Println("Execerr:", exerr, "Other var:", n, "statement:", iterexec)
			w.Write([]byte("{\"DB ERROR:\":\"Failure to insert. INSERT ERROR FROM DB HERE.\""))
		}

		//This code block will get slower and slower as the database grows. Are we sure we want to do this?
		res, err := db.Query(fmt.Sprintf("SELECT upload_id FROM upload_master_nodes"))
		if err != nil {
			//fmt.Println(fmt.Errorf("couldn't get a master node index. An hero."))
			//os.Exit(1)
			fmt.Println("Error getting master node index: ", err)
		} else {
			for res.Next() {
				res.Scan(&it)
				//if it > masterIndexLast { //Really???
				masterIndexLast = it
				////fmt.Println(masterIndexLast)
				//}
			}
		}

		masterIndexLastStr := strconv.Itoa(int(masterIndexLast + 1))
		retarr = []byte("{\"NDX\":" + masterIndexLastStr + "}")
		w.Write(retarr)

	}
}

/*
	Handle being able to recieve different types of data via AJAX
	using Go generics. This is probably some form of abuse.

	Note: We need to be able to communicate to the client if the database manager function fails
	to store the data so that it can be re-sent if necessary. This will probably involve creating a custom
	type so that we can pass a reference to the DBMF to write to the writer.
*/
func templFunc[V dipStruct | fuelStruct | kratomStruct | moodStruct | reciptStruct | noteStruct | locationStruct | timeStruct](output chan jsonElem, varStr V) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		ds := dipStruct{}
		fs := fuelStruct{}
		ks := kratomStruct{}
		ms := moodStruct{}
		ns := noteStruct{}
		rs := reciptStruct{}
		ls := locationStruct{}

		var retStr jsonElem = jsonElem{}

		if reflect.TypeOf(ds) == reflect.TypeOf(varStr) { //IF DIP DATA||||||||||||||||||||||||||
			if err := json.NewDecoder(r.Body).Decode(&ds); err != nil {
				handleError(err, w)
			}

			retStr.DipStruct = ds
			////fmt.Printf("\n=======================\n\nDip Struct Decoded: \n%v\n\n=====================\n", retStr.DipStruct)
			w.Write([]byte("{\"DATA\":\"WORKING\""))

		} else if reflect.TypeOf(fs) == reflect.TypeOf(varStr) { //IF FUEL DATA||||||||||||||||||
			fmt.Print(fmt.Sprintf("\n==========\nDecoding Fuel Data\n"))
			if err := json.NewDecoder(r.Body).Decode(&fs); err != nil {
				handleError(err, w)
			}
			retStr.FuelStruct = fs
			fmt.Print(fmt.Sprintf("fs is %v \n============\n", fs))

		} else if reflect.TypeOf(ks) == reflect.TypeOf(varStr) { //IF KRATOM DATA|||||||||||||||||
			////fmt.Println("Entering kratom stuff.")
			if err := json.NewDecoder(r.Body).Decode(&retStr.KratomStruct); err != nil {
				handleError(err, w)
			}

			////fmt.Println("KRATDAT", retStr.KratomStruct)
			output <- retStr

		} else if reflect.TypeOf(ms) == reflect.TypeOf(varStr) { //IF MOOD DATA |||||||||||||||||
			if err := json.NewDecoder(r.Body).Decode(&retStr.MoodStruct); err != nil {
				handleError(err, w)
			}
		} else if reflect.TypeOf(ns) == reflect.TypeOf(varStr) { //NOTESSSS     |||||||||||||||||

			if err := json.NewDecoder(r.Body).Decode(&retStr.Note); err != nil {
				handleError(err, w)
			}

			w.Write([]byte("{\"Message\":\"Got IT\""))

			//fmt.Print("\n=============================\nGood note decode:\n", retStr.Note, "\n=============================\n")

		} else if reflect.TypeOf(rs) == reflect.TypeOf(varStr) { //RECIPTS      ||||||||||||||||||||||||||||||
			if err := json.NewDecoder(r.Body).Decode(&retStr.ReciptStruct); err != nil {
				handleError(err, w)
			}
		} else if reflect.TypeOf(ls) == reflect.TypeOf(varStr) {
			if err := json.NewDecoder(r.Body).Decode(&retStr.LocationStruct); err != nil {
				handleError(err, w)
			}
		}

		output <- retStr
		//fmt.Print("\n====================\nRetstr template function output:", retStr, "\n====================\n")
	}
}

func handleError(input error, w http.ResponseWriter) {
	//fmt.Print("\n\nERR:\n", input, "\n\n")
	returnText := fmt.Sprintf("Error returned from server: %s", input.Error())
	fmt.Print(fmt.Sprintf("\n=====<<<<<<<<<\n%s\n=====<<<<<<<<<\n", returnText))
	w.Write([]byte(returnText))
}

func genRoot() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		fileDat, _ := ioutil.ReadFile("./index.html")
		w.Header().Set("Content-Type", "text/html")
		w.Write(fileDat)

		////fmt.Printf("\n\n===============%v\n===============\n\n", string(fileDat))
	}
}

func databaseManager(input chan jsonElem) {
	db, err := sql.Open("mysql", "root:55975597@tcp(173.211.12.46:3306)/umwelt_22")
	if err != nil {
		//fmt.Println("Err starting db connection:", err)
	}

	var version string
	err2 := db.QueryRow("SELECT VERSION()").Scan(&version)

	if err2 != nil {
		//fmt.Println("Query Error:", err2)
	}
	//fmt.Println("VERSION:", version)

	ds := dipStruct{}
	ks := kratomStruct{}
	ns := noteStruct{}
	fs := fuelStruct{}
	ts := timeStruct{}
	//fs := fuelStruct{}

	for recMsg := range input {
		//fmt.Println("NoteStruct:", recMsg.Note)

		//var masterIndexLast int = 0
		if recMsg.DipStruct != ds { //ENTERING DIP DATA
			ds := recMsg.DipStruct
			sqlStatement := fmt.Sprintf("INSERT INTO dip(dip_id,location,time,brand,mouth_map_x,mouth_map_y,user,master,dip_in) VALUES (%d,%d,%d,%d,%g,%g,%d,%d,\"%s\")", ds.ID, ds.LocationId, ds.Timestamp, ds.BrandId, ds.MouthMapX, ds.MouthMapY, ds.UserId, ds.MasterID, ds.DipIn)

			_, err2 := db.Exec(sqlStatement)

			if err2 != nil {
				//fmt.Printf("\n\n==========\nISSUE IN EXECUTING SQL STATMENT AT databaseManager:   %v\nstatement: %v\nErrCode:%d===========\n\n", err2, sqlStatement)
			} else {
				//fmt.Printf("\nNo error entering SQL data for master index %d\n", masterIndexLast+1)
			}
		}

		if recMsg.KratomStruct != ks { //ENTERING KRATOM DATA
			ks = recMsg.KratomStruct
			//fmt.Print(fmt.Sprintf("\n================================\nKS:\n%v\n%t============================\n\n", ks, ks))
			sqlStatement := fmt.Sprintf("INSERT INTO kratom_usage(amount_used_ml, amount_used_grm, location_id, time_link, vein_color, user_id, master) VALUES (%d, %d, %d, %d, \"%s\", 0, %d);", ks.MilliterslUsed, ks.GramsUsed, ks.LocationId, ks.TimestampLink, ks.VeinColor, ks.MasterID)
			_, err := db.Exec(sqlStatement)
			if err != nil {
				//fmt.Printf("\n\n==========\nISSUE IN EXECUTING SQL STATMENT AT databaseManager:   \n%v\nstatement: %v\nErrCode:%d\n===========\n\n", err2, sqlStatement)
			} else {
				//fmt.Printf("\nNo error entering SQL data for master index %d\n", masterIndexLast+1)
			}
		}

		if recMsg.Note != ns {
			//                                               tit       | body    |  time   |   loc     | masta         |t||b||tm||l||m
			sqlStatement := fmt.Sprintf("INSERT INTO notes(note_title,note_body,note_time,note_location,master) VALUES (\"%s\",\"%s\",%d,%d,%d);", recMsg.Note.Title, recMsg.Note.Body, recMsg.Note.Time, recMsg.Note.Location, recMsg.Note.MasterID)
			_, err := db.Exec(sqlStatement)
			if err != nil {
				//fmt.Print("BIG PROBLEM MAKING DB FOR NOTES GO VROOM!:", err)
			} else {
				//fmt.Println("Good execution on DB:", sqlStatement)
			}
		}

		//Enter fuel data into database=============================================================================================================================
		if recMsg.FuelStruct != fs {
			//fmt.Print(fmt.Sprintf("Fuel struct is %v\n", recMsg.FuelStruct))
			fs = recMsg.FuelStruct

			sqlStmt := fmt.Sprintf("INSERT INTO fuel_data (grade,  gallons,  price_per_gallon,  odomiter,  gas_station_company,  total_price,  location_link,  time_link,  master_id) VALUES (\"%s\",%g,%g,%g,\"%s\",%g,%d,%d,%d);", fs.Grade, fs.Gallons, fs.PricePerGallon, fs.Odometer, fs.GasStationCompany, fs.Price, fs.LocationID, fs.TimestampLink, fs.MasterID)
			_, err := db.Exec(sqlStmt)
			if err != nil {
				fmt.Println("FUEL ENTRY FAILED: ", err, "\nSQL statment was:", sqlStmt)
			} else {
				fmt.Println("FUEL IN DB GOOD. Statment was: ", sqlStmt)
			}
		}

		//| time_id | unix_timestamp | datetime                                                | master |
		if recMsg.TimeStruct != ts {
			ts = recMsg.TimeStruct
			sqlStmt := fmt.Sprintf("INSERT INTO time (unix_timestamp, datetime, master) VALUES (%d,\"%s\", %d);", ts.Unix_Timestamp, ts.DateTime, ts.MasterID)
			_, err := db.Exec(sqlStmt)
			if err != nil {
				fmt.Println("TIMESTAMP PUT:", ts, "Statement was: ", sqlStmt)
			} else {
				fmt.Println("TS IN DB GOOD:", sqlStmt)
			}
		}

	}

}

/**
type fuelStruct struct {
	ID                uint64
	Grade             string
	Gallons           float32
	PricePerGallon    float32
	Odometer          float32
	GasStationCompany string
	Price             float32
	LocationID        uint64
	TimestampLink     uint64
	MasterID          uint64
}

*/
